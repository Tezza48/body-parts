﻿#pragma strict

import UnityEngine.UI;


public var textBank : textBank;
public var writing : boolean;//so you can only write if the write string has finished
public var interupt : boolean = false;

private var textFormatted : String;
private var textText : UI.Text;
private var tickTime : float = 0.05;
private var fadeTime : float = 10.0;
private var i : int = 0;//for while loop

function Start () {
	textText = GetComponent(Text);
	for (i in textBank.textStart){//write the starting text to the screen
		if (interupt) break;
		while (writing) {
			yield WaitForEndOfFrame;//stops while loop from being a 
		}
		WriteToString(i);
	}
}

function Interupt () {
	interupt = true;
	yield WaitForSeconds (1);
	interupt = false;
}

function WriteToString (textIn:String){
/*	write text to a text Mesh
-	-	-	-	-	-	-	-	-	-	*/
	textText.text = "";//clears preveous text (just in case)
	writing = true;//allows outside scripts to check whether this is alreadt writing to avoid confusing it.
//var textCharPos : int = 0;
//var charInLine : int = 0;
	// run through the given string letter by letter and add the letter to the text mesh string
	for (var i in textIn) {
		if (interupt) {
			//interupt = false;
			textFormatted = "";
			textText.text = "";
			break;
		}
		
		var waitTime = (i == "." || i == ",") ? tickTime * 3 : tickTime;//wait a little longer for punctuation.
		
		// checks to see if the curent letter is a word and that the string is shorter than the max length for a line.
		// no longer needed with new UI. yay
		/*if (charInLine >= 64 && i == " ") {
			//adds a new line when the string is a certain length
			//textFormatted += i + "\n";
			//charInLine = 0;
		} else {
		}*/
		textFormatted += i;//add the processed character to the formatted text
		
		textText.text = textFormatted;//update the displayed text with the new string
		
		// charInLine ++;
		yield WaitForSeconds(waitTime);//delay after writing a letter.
	}
	yield WaitForSeconds((interupt) ? 0 : .5);//leave the text up for a short while so it can be read.
	textFormatted = "";//clear the strings when finished just to be safe
	textText.text = "";
	writing = false;//no longer writing
}
