﻿#pragma strict

public var player : Transform;
private var lerpTime : float = 5;

function Start () {
	player = GameObject.Find("Player").GetComponent(Transform);
}

function Update () {
	transform.position = Vector2.Lerp(transform.position, player.position, lerpTime * Time.deltaTime);//make te camera follow the player smoothly
}