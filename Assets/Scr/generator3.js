﻿#pragma strict
/*
0 = space.
1 = centre of a room.
2 = room tile.
3 = corridor tile.
-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-*/
public var wall : GameObject;//
private var width : int = 25;
private var height : int = 25;
private var grid = new int[width, height];//array which holds the map data, this is used to create gridReal.
private var gridReal = new GameObject[width * height];//array of actual game objects creating the dungeon
public var dx : float = 5.12;//width of tile.
public var dy : float = 5.12;//height of tile.

private var roomSizex : int = 3;
private var roomSizey : int = 3;
private var roomNumber : int = 10;
private var tryLimitMax = 100;
private var corridorTryDistance = 5;
private var x : int;//for for loops
private var y : int;
private var rpx : int;//room position x and y, used for marking the centre of the next room
private var rpy : int;
private var corridorDir : short;//direction to make a corridor in.

function Start () {
	GenMap ();//initial map generation, spawns in the first room
	CreateMap ();
}

function GenMap (){
	var tryLimit = tryLimitMax; //	limits the number of tries to prevent an infinite loop
	var roomsLeft = roomNumber;
	rpx = new Random.Range(2, width- roomSizex - 3);//	random position x
	rpy = new Random.Range(2, height- roomSizey - 3);//	random position y
	for(x = 0; x < roomSizex; x++){//place a room marker in the tiles that the room will fill
		for(y = 0; y < roomSizey; y++){
			grid[x, y] = 2;
		}
	}
	grid[rpx, rpy] = 1;
	//GenCorridor();
}

function GenCorridor(){//make a corridoor from the centre of the room
	var i : int;
	var j : int;
	for(i = 4; i < 0; i--) {
		corridorDir = new Random.Range(0, 3);//clockwise starting from 12 o'clock
		var tryDir : int = new Random.Range(0, 1);//if this fails, which direction to try next
		//check that you can acually put a corridoor there
		//2 + corridor length + 4
		if (corridorDir == 0) {
			if (rpy + 6 + corridorTryDistance < height) {
				for(j = 0; j < corridorTryDistance; j++) {
					grid[rpx, rpy + 2 + j] = 3;
				}
				rpy += 6 + corridorTryDistance;
			}
		}
		if (corridorDir == 1) {
			
		}
		if (corridorDir == 2) {
			
		}
		if (corridorDir == 3) {
			
		}
	}
}

function CreateMap () {
	for (var x = 0; x < width; x++) {
		for (var y = 0; y < height; y++){
			var wallObj = Instantiate(wall, new Vector2 (dx * x, dy * y), transform.rotation);
			wall.name = "(" + x + ", " + y + "): " + grid[x, y];
			gridReal[x * height + y] = wall;
			wall.SetActive(grid[x, y] == 0 ? true : false);
		}
	}
}
/*	
-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-*/
function IsValidRoom (rpx : int, rpy : int, roomSizex : int, roomSizey : int) {
	//
	var u = rpy + roomSizey - 1;
	var r = rpx + roomSizex - 1;
	var d = rpy - roomSizey + 1;
	var l = rpx - roomSizex + 1;
	if (grid[rpx, u] == 0 && grid[r, rpy] == 0 && grid[rpx, d] == 0 && grid[l, rpy] == 0 && grid[r, u] == 0 && grid[r, d] == 0 && grid[l, d] == 0 && grid[l, u] == 0) {
		//Debug.Log("room check");
		return true;
	} else {
		//Debug.LogError("room check");
		return false;
	}
}