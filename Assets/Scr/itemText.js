﻿#pragma strict
//designed to hold the pickup text for items/body parts
public var textController : textController;
public var pickupText : String;

function OnTriggerEnter2D (coll : Collider2D) {
	if (coll.tag == "Player")
		textController.WriteToString(pickupText);
}