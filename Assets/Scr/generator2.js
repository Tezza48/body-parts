﻿#pragma strict
/*
0 = space.
1 = floor tile.
2 = perspective wall.

TO DO:
	check wether created coridoor will be within the bounds of te array
	check space the coridoor will fill
	create coridoor
	
-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-*/
public var wall : GameObject;
private var width : int = 20;
private var height : int = 20;
private var grid = new int[width, height];
private var rooms = new int[roomNumber,roomNumber];
private var gridReal = new GameObject[width * height];
private var roomSizex : int = 3;
private var roomSizey : int = 3;
private var roomNumber : int = 10;
private var tryLimitMax = 100;
private var coridoorTryDistance =5;
private var coridoorLength = 5;
private var dx : float = 5.12;
private var dy : float = 5.12;

function Start () {
	GenerateMap ();
	CreateMap ();
}

/*	Generate a map of set width and height with
-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-*/
function GenerateMap () {
	// make an array of 0s
	for(var x = 0; x < width; x++){
		for (var y = 0; y < height; y++) {
			grid[x, y] = 0;
		}
	}
	
	var rpx : int;//room pos x
	var rpy : int;//room pos y
	var tryLimit = tryLimitMax; //	limits the number of tries to prevent an infinite loop
	var roomsLeft = roomNumber;
	rpx = new Random.Range(2, width- roomSizex - 3);//	random position x
	rpy = new Random.Range(2, height- roomSizey - 3);//	random position y
	
	// make a room starting at that position
	for (x = 0; x < roomSizex; x++) {
		for (y = 0; y < roomSizey; y++) {
			grid[rpx + x, rpy + y] = 1;//make initail room.
		}
	}
	roomsLeft--;
	var randomWall : int;
	var wallcounter : int;
	var validWall : boolean = false;// for looping wall selection
	var direction : int;//	direction that a coridoor is facing;
	var xx : int;//vars to use in nested for loops.
	var yy : int;
	var selectedTile : int[];
	while (tryLimit > 0) {
		//	pick a random wall
		wallcounter = 0;
		randomWall = new Random.Range(0, (roomSizex + roomSizey) * 2);
		for(x = 1; x < width - 1; x++){
			for (y = 1; y < height - 1; y++) {
				// is this a wall (if so assign it's direction).
				if (grid[x, y - 1] == 1) {
					direction = 0;
				} else if (grid[x - 1, y] == 1) {
					direction = 1;
				} else if (grid[x, y + 1] == 1) {
					direction = 2;
				} else if (grid[x + 1, y] == 1) {
					direction = 3;
				} else {
					x = width;
					y = height;
				}
				if (grid[x, y] == 1 && wallcounter == randomWall) {//if this wall is the random one selected, 
					switch (direction) {
						case 0:
							//	check that a coridoor can be made from that wall
							if (y + coridoorLength >= height + 1) continue;//restart the while loop if that wall would go out of the bounds of the array
							for (yy = 0; yy < coridoorLength; yy++) {
								if (grid[x, y + yy] == 1) continue;//restart the while loop if the wall hits another room
							}
							//	make a coridoor;
							for (yy = 0; yy < 3; yy++) {
								grid[x, y + yy] = 1;
							}
							break;
						case 1:
							if (x + coridoorLength >= width + 1) continue;
							for (xx = 0; xx < coridoorLength; xx++) {
								if (grid[x + xx, y] == 1) continue;
							}
							for (xx = 0; xx < coridoorLength; xx++) {
								grid[x + xx, y + yy] = 1;
							}
							break;
						case 2:
							if (y + coridoorLength < 1) continue;
							for (yy = 0; yy < coridoorLength; yy++) {
								if (grid[x, y - yy] == 1) continue;
							}
							//	make a coridoor;
							for (yy = 0; yy < 3; yy++) {
								grid[x, y + yy] = 1;
							}
							break;
						case 3:
							if (x - coridoorLength < 1) continue;
							for (xx = 0; xx < coridoorLength; xx++) {
								if (grid[x - xx, y] == 1) continue;
							}
							for (xx = 0; xx < coridoorLength; xx++) {
								grid[x - xx, y] = 1;
							}
							break;
					}
					x = width;
					y = height;
				} else {
					wallcounter++;
				}
			}
		}
		tryLimit--;
	}
}

function CreateMap () {
	for (var x = 0; x < width; x++) {
		for (var y = 0; y < height; y++){
			var wallObj = Instantiate(wall, new Vector2 (dx * x, dy * y), transform.rotation);
			wall.name = "(" + x + ", " + y + "): " + grid[x, y];
			gridReal[x * height + y] = wall;
			wall.SetActive(grid[x, y] == 0 ? true : false);
		}
	}
}
/*	
-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-*/
function IsValidRoom (rpx : int, rpy : int, roomSizex : int, roomSizey : int) {
	//
	var u = rpy + roomSizey - 1;
	var r = rpx + roomSizex - 1;
	var d = rpy - roomSizey + 1;
	var l = rpx - roomSizex + 1;
	if (grid[rpx, u] == 0 && grid[r, rpy] == 0 && grid[rpx, d] == 0 && grid[l, rpy] == 0 && grid[r, u] == 0 && grid[r, d] == 0 && grid[l, d] == 0 && grid[l, u] == 0) {
		//Debug.Log("room check");
		return true;
	} else {
		//Debug.LogError("room check");
		return false;
	}
}