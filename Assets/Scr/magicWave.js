﻿#pragma strict

public var tTimer : float;
public var lifeTime : float = 1;

function Start () {
	tTimer = Time.time + lifeTime;
}

function Update () {
	if (Time.time >= tTimer) {//destroy the wave after a short ammount of time
		Destroy(gameObject);
		Destroy(this);
	}
}