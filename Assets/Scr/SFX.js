﻿#pragma strict

public var whoosh : AudioClip;
public var snap : AudioClip;

function SFXWhoosh () {
	GetComponent(AudioSource).clip = whoosh;//play the whoosh sound
	GetComponent(AudioSource).loop = false;
	GetComponent(AudioSource).Play();
}
function SFXSnap () {
	GetComponent(AudioSource).clip = snap;//loop the snap sound
	GetComponent(AudioSource).loop = true;
	GetComponent(AudioSource).Play();
}