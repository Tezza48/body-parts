﻿#pragma strict

public var head : boolean;
public var torso : boolean;
public var arms : boolean;
public var legs : boolean;
public var legsMultiplier : float = 1.5;
public var textBank : textBank;
public var textController : textController;
public var heart : GameObject;
//text to display when picking up something
private var headText : String = "Being able to concentrate will help me to defend myself";
private var torsoText : String = "I should be able to sustain more damage now";
private var armsText : String = "My attacks will be more powerful now";
private var legsText : String = "Movement is Key to survival";

public var character : GameObject[];

function Start () {
}

function Update () {
	GetComponent(playerControl).heal = (torso) ? true : false;//torso lets you heal
	GetComponent(playerControl).waveCooldownMax = (head) ? 1.5 : 3.0;//wave cooldown is halved if you have head
	GetComponent(playerControl).moveSpeedMultiplier = (legs) ? legsMultiplier : 1;//move faster if you have legs
	//and arms make you do a big ring attack...
	if (!heart.activeSelf && head && torso && arms && legs) heart.SetActive (true);//make the heart active when you have collected all of the body parts.
}
/*	-	-	-	-	-	-	-	-	-	-	-	-	-	
	these functions activate the check booleans and
	turn on the respective body part sprites*/
function Head (active : boolean, other : Collider2D) {
	head = active;
	character[0].SetActive(active);
	other.gameObject.SetActive(false);
	textController.Interupt ();
	yield WaitForSeconds (2);
	textController.WriteToString (textBank.headText);
}
function Torso (active : boolean, other : Collider2D) {
	torso = active;
	character[1].SetActive(active);
	other.gameObject.SetActive(false);
	textController.Interupt ();
	yield WaitForSeconds (2);
	textController.WriteToString (textBank.torsoText);
}
function Arms (active : boolean, other : Collider2D) {
	arms = active;
	character[2].SetActive(active);
	other.gameObject.SetActive(false);
	textController.Interupt ();
	yield WaitForSeconds (2);
	textController.WriteToString (textBank.armText);
}
function Legs (active : boolean, other : Collider2D) {
	legs  = true;
	character[3].SetActive(active);
	other.gameObject.SetActive(false);
	textController.Interupt ();
	yield WaitForSeconds (2);
	textController.WriteToString (textBank.legText);
}

function OnTriggerEnter2D (other : Collider2D) {//turn on the effects of the body parts and their sprites
	if (other.name == "head") Head (true, other);
	if (other.name == "torso") Torso (true, other);
	if (other.name == "arms") Arms (true, other);
	if (other.name == "legs") Legs (true, other);
}