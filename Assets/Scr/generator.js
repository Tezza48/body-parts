﻿#pragma strict
/*	turns an array into a map, basic map tool.
	  = wall
	# = floor
	P = player
	E = enemy
-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-*/
public var wall : GameObject;
public var edgeing : GameObject[];
public var player : Transform;
public var cameraHolder : Transform;
public var enemy : GameObject;
private var width : int = 20;
private var height : int = 20;
private var grid : String[] = [	" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
								" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ",
								" "," "," "," "," "," "," "," "," "," "," ","#","E","#"," "," "," "," "," "," ",
								" ","#","#","#","#","#"," ","#","E","#","#","#","#","#","#","#","#"," "," "," ",
								" ","#"," ","#","#","#","#","#"," "," "," ","#","#","#"," "," ","#"," "," "," ",
								" ","#"," ","#","E","#"," ","#"," "," "," "," "," ","#"," ","#","#","#"," "," ",
								" ","#"," "," "," "," "," ","#"," ","#","#","E"," ","#"," ","#","#","#"," "," ",
								" ","#","#","E"," ","#","#","#"," ","#","E","#"," ","#"," ","#","#","#"," "," ",
								" ","#","#","#"," ","#","#","#"," ","#","E","#"," ","#"," "," "," ","#"," "," ",
								" ","#","#","#"," ","#","#","#"," ","#"," "," "," ","#"," "," "," ","#"," "," ",
								" "," ","#"," "," ","#"," "," ","#","#","#"," "," ","#"," ","#","#","#"," "," ",
								" "," ","#","#","#","#"," "," ","E","#","#","#","E","#","#","#","E","#"," "," ",
								" "," "," ","E"," "," "," "," ","#","#","#"," ","#"," "," ","#","#","#"," "," ",
								" "," ","#","#","#","#","#"," "," "," "," "," ","#"," "," "," "," "," "," "," ",
								" "," ","#","#","#"," ","E","#","#"," "," ","#","#","#"," "," "," "," "," "," ",
								" "," ","#","#","#"," ","#","#","#"," "," ","#","P","#"," "," "," "," "," "," ",
								" "," "," "," "," "," ","#","#","#"," "," ","#","#","#"," "," "," "," "," "," ",
								" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","╔","╦","╗",
								" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","╠"," ","╣",
								" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ","╚","╩","╝"];
private var showGrid = new GameObject[width * height];
private var dx : float = 5.12;//width and height of the wall blocks
private var dy : float = 5.12;


function Start () {
	player = GameObject.Find("Player").GetComponent(Transform);
	cameraHolder = GameObject.Find("CameraHolder").GetComponent(Transform);
	CreateMap ();
}
function Update () {
}
/*	make a grid of gameobjects using values in grid[].
-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-*/
function CreateMap () {
	var cell : GameObject;
	for (var x = 0; x < width; x++) {
		for (var y = 0; y < height; y++){
			if (grid[y * width + x] == " ") {
				Instantiate(wall, new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "P") {
				player.position = new Vector2 (dx * x, -dy * y);
			} else if (grid[y * width + x] == "E") {
				Instantiate (enemy,  new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "╠") {
				Instantiate(edgeing[0], new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "╣") {
				Instantiate(edgeing[1], new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "╦") {
				Instantiate(edgeing[2], new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "╩") {
				Instantiate(edgeing[3], new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "╚") {
				Instantiate(edgeing[4], new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "╝") {
				Instantiate(edgeing[5], new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "╔") {
				Instantiate(edgeing[6], new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == "╗") {
				Instantiate(edgeing[7], new Vector2 (dx * x, -dy * y), transform.rotation);
			}/* else if (grid[y * width + x] == " ") {
				Instantiate(edgeing[8], new Vector2 (dx * x, -dy * y), transform.rotation);
			} else if (grid[y * width + x] == " ") {
				Instantiate(edgeing[9], new Vector2 (dx * x, -dy * y), transform.rotation);
				
			}*/
		}
	}
}
