﻿#pragma strict

public var angle : float;//roation of the character sprites
public var canMove : boolean = true;
public var Game : GameObject;
public var heal : boolean = false;
public var healAmmount : float = 2;
public var health : float = 100.0;
public var healthDrain : float = 8;
public var magicWavePrefab : GameObject;
public var magicRingPrefab : GameObject;
public var moveSpeed : float = 5.0;
public var moveSpeedMultiplier : float = 0;
public var waveForce : float = 10;
public var waveCooldownGUIPozMin : float;
public var waveCooldownGUIPozMax : float;
public var waveCooldownGUI : Transform;
public var waveCooldownMax : float = 3.0;
public var waveCooldown : float = 0;
public var SFX : SFX;

private var anim : Animator;
private var facingDirection : int;
private var healthMax : float = 100.0;

function Start () {
	Game = GameObject.Find("Game");
	waveCooldownGUI = GameObject.Find("Wave Cooldown").GetComponent(Transform);
	anim = gameObject.Find("character").GetComponent(Animator);
}

function Update () {
	if (waveCooldown <= 0) {//if the wave cooldown is finished fire a wave in the direction pressed
		if (Input.GetKeyDown("up"))
			FireMagicWave(0);
		if (Input.GetKeyDown("right"))
			FireMagicWave(1);
		if (Input.GetKeyDown("down"))
			FireMagicWave(2);
		if (Input.GetKeyDown("left"))
			FireMagicWave(3);
	}
	
	if (waveCooldown > 0) waveCooldown -= Time.deltaTime;
		
	waveCooldownGUI.localPosition.x = waveCooldownGUIPozMin * (waveCooldown)/waveCooldownMax;//position the cooldown bar
	
	if (heal && health < healthMax) Heal(healAmmount, false);//heal the player if heal is activated and the health is lower than the max health
	
	if (transform.localScale.x >= 0 && transform.localScale.y > 0 ) {//make the spirit smaller as it takes damage
		gameObject.transform.localScale.x = health/100 + 0.1;//+.1 to make sure it dosn't clip between the colliders.
		gameObject.transform.localScale.y = health/100 + 0.1;
	}
	if (health <= 0) {
		Die ();
		health = 100000;//stop die from being called multiple times and messing with the text controller
	}
	
}

function FixedUpdate () {
	if (canMove) Move();//allows me to stop the player from moving
	rigidbody2D.velocity = Vector2.zero;
}

function Die () {//execute the death scenario
	Game.GetComponent(GameController).FadeOut();
	Game.GetComponent(GameController).textController.Interupt ();
	GetComponent(Transform).position = Vector2(-100, -100);
	yield WaitForSeconds (2);
	Game.GetComponent(GameController).PlayerDeath();
}

function FireMagicWave (direction : int) {
	SFX.SFXWhoosh(); //play whoosh noise when you fire.
	waveCooldown = waveCooldownMax;//set the cooldown of the wave attack
	var wave : GameObject;
	if (GetComponent(inventory).arms) {
		wave = Instantiate(magicRingPrefab, transform.position, transform.rotation);
	} else {
		switch (direction) {//shoot the wave in the direction that was pressed
			case 0:
				wave = Instantiate(magicWavePrefab, transform.position, transform.rotation * Quaternion.Euler (0, 0, 0));
				wave.rigidbody2D.AddForce (Vector2.up * waveForce, ForceMode2D.Impulse);
				break;
			case 1:
				wave = Instantiate(magicWavePrefab, transform.position, transform.rotation * Quaternion.Euler (0, 0, -90));
				wave.rigidbody2D.AddForce (Vector2.right * waveForce, ForceMode2D.Impulse);
				break;
			case 2:
				wave = Instantiate(magicWavePrefab, transform.position, transform.rotation * Quaternion.Euler (0, 0, 180));
				wave.rigidbody2D.AddForce (-Vector2.up * waveForce, ForceMode2D.Impulse);
				break;
			case 3:
				wave = Instantiate(magicWavePrefab, transform.position, transform.rotation * Quaternion.Euler (0, 0, 90));
				wave.rigidbody2D.AddForce (-Vector2.right * waveForce, ForceMode2D.Impulse);
				break;
		}
	}
}

function Heal (rate : float, bulk : boolean) {
	//heal a ceartain ammount.
	//if it's a small ammout off full health, or over, make health equal the max
	if (health + rate <= healthMax) {
		health += (heal) ? rate * ((bulk) ? 1 : Time.deltaTime) : 0;
	} else if (health > healthMax) {
		health = healthMax;
	}
}

function Move () {//move the player in the axis pressed
	transform.position.x += Input.GetAxis("Horizontal") * moveSpeed * moveSpeedMultiplier * Time.deltaTime;
	transform.position.y += Input.GetAxis("Vertical") * moveSpeed * moveSpeedMultiplier * Time.deltaTime;
	
	var moving : boolean = (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) ? true : false;
	anim.SetBool("Moving", moving);//put the animator into the moving state when moving and standing when not
	
	if (moving)//get the direction you are moving in
		angle = (Mathf.Atan2 (-Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * Mathf.Rad2Deg);
		
	for (var i in GetComponent(inventory).character) { //rotate the character in the direction they are moving.
		i.transform.rotation = Quaternion.Euler(0, 0, angle);
	}
}

function OnCollisionStay2D (coll : Collision2D) {
	if (coll.gameObject.tag == "Enemy") {
		health -= healthDrain * Time.deltaTime;//drain the player's health over time whilst in contact with enemies
	}
}

function OnCollisionEnter2D (coll : Collision2D) {
	if (coll.gameObject.tag == "Enemy") {
		SFX.SFXSnap ();//make the snap sound effect when touching enemies
	}
}

function OnCollisionExit2D (coll : Collision2D) {
	if (coll.gameObject.tag == "Enemy") {
		SFX.GetComponent(AudioSource).Stop ();//stop the sfx when not touching an enemy
	}
}