﻿#pragma strict

public var menu : GameObject;
public var paused : boolean = false;
public var fadeOutImage : UnityEngine.UI.Image;
public var fadeOut : boolean;
@Range (0.0f, 3.0f)
public var fadeTime : float;
public var textController : textController;

function Start () {
	//menu = GameObject.Find("Menu");
}

function Update () {
	if (Input.GetButtonUp("Cancel")) Pause();//pause the game
	Time.timeScale = (paused) ? 0.0 : 1.0;
	if (fadeOut) fadeOutImage.color = Color.Lerp(Color(0, 0, 0, 0), Color(0, 0, 0, 1), Time.time / fadeTime);//fade the screen to black
	//fadeOutImage.color = (fadeOut) ? Color.Lerp(Color(0, 0, 0, 0), Color(0, 0, 0, 1), Time.time / fadeTime) : Color(0, 0, 0, 0);
}

function Pause () {//pause menu, no longer in use.
	if (menu.activeSelf) {
		menu.SetActive (false);
		paused = false;
	} else if (!menu.activeSelf) {
		menu.SetActive (true);
		paused = true;
	}
}

function FadeOut () {//start fading
	fadeOut = true;
}

function PlayerDeath() {
	FadeOut();
	textController.Interupt ();
	yield WaitForSeconds (2);
	textController.WriteToString (GetComponent(textBank).deathText);
	GetComponent(Transform).position = Vector2(-10, -10);//move the player out of the map so it cannot keep getting hit by enemies
	yield WaitForSeconds (15);
	GetComponent(loadLevel).LoadLevel();//load the menu screen
}