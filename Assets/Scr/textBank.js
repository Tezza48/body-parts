﻿#pragma strict

// this script contains various bits of text to be used by the text controller.

public var textStart : String[] = [
"What are you when you are nothing?",
"You are mearly a spirit, a wisp, a figment of the past...",
"Your body is lost to the world, you are defenseless.",
"You must regain your body...",
"And become something again"];//text used at the start of the level
public var legText : String = "Being able to move quickly should protect me.";
public var armText : String = "My magic depended on my hands, wait until they see me now";
public var headText : String = "My thoughts are clearer, i can now concentrate on my counter attacks.";
public var torsoText : String = "I feel stronger.";
public var deathText : String = "Negative energy has gotten the best of you, you are no more.";
public var heartText : String[] = [
"Blood surges through my body, a sensation I have long forgotten...",
"I am being filled with a power I recall from a previous life...",
"I... Gain... Strength...",//cue a fade
"Something inside of me is growing, trying to escape.",
"I am becoming Something again...",
"Something that I cannot contain."];//cue menu screen