﻿#pragma strict

public var GameController : GameController;
public var playerControl : playerControl;
public var textController : textController;
private var tTime : float;

function Start () {

}

function Update () {

}

function HeartText (){//trigger the game win text and que blackout
	for (var i = 0; i < textController.textBank.heartText.length; i++) {
		while (textController.writing) {
			yield WaitForEndOfFrame;
		}
		textController.WriteToString(textController.textBank.heartText[i]);
		if (i == 3) GameController.FadeOut();
	}
	yield WaitForSeconds (5);
	GameController.GetComponent(loadLevel).LoadLevel();//go to the menu screen once the text is finished
}

function OnTriggerEnter2D (coll : Collider2D) {
	if (coll.tag == "Player") {
		playerControl.canMove = false;//stop the player from moving and start rolling the end game text
		HeartText ();
		gameObject.collider2D.enabled = false;
	}
}