﻿#pragma strict
/*	-	-	-	-	-	-	-	-	-	-	-	
	enemy follows toe player when in aggro
	distance and takes two hits from magic
	wave to kill
*/
private var player : GameObject;
public var moveSpeed : float = 2.5;
public var attackDamage : float = 2;
private var hit : boolean;
private var dead : boolean;
private var deathTime : float = 1.5;
private var tTimer : float;
private var anim : Animator;

function Start () {
	anim = GetComponent(Animator);
	player = GameObject.Find("Player");
}

function FixedUpdate () {
	if (!dead)//follow the player if it's alive
		FollowPlayer ();
}

function FollowPlayer() {//move towards the player if it's within aggro range
	var direction : Vector2 = player.transform.position - transform.position;
	var distance = direction.magnitude;
	if (distance < 8) {
		rigidbody2D.velocity = direction.normalized * moveSpeed;
	} else {
		rigidbody2D.velocity = Vector2.zero;
	}
}

function OnCollisionEnter2D (coll : Collision2D) {
	if (coll.gameObject.tag == "Wave") {
		if (hit) {//the enemy has been hit once already
		anim.SetBool("Dead", true);//trigger death animation
		Kill();
		} else {
			hit = true;//the enemy has taken one hit, the next will kill it
		}
	}
}

function Idle () {
	
}

function Kill () {
	dead = true;
	yield WaitForSeconds (deathTime);
	GetComponent(AudioSource).mute = true;
	Destroy(gameObject);
	Destroy(this);
}